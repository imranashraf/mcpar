/*
 *
 * Authors: Imran Ashraf
 *
 */

#include "taskgraph.h"

extern TaskDependenceMatrix tdMatrix;

tgTreeType Taskgraph::GetNodeWithSubtree(cgTreeType::sibling_iterator sIter, cgTreeType& cgt)
{
    tgTreeType::iterator i1;
    tgTreeType st1;

    int nChild = sIter.number_of_children();
    D1ECHO( *sIter << " " << VAR(nChild) );

    tgTreeType subTree1;
    if(nChild == 0) // no children
    {
        D1ECHO("Leaf case for " << *sIter);
        subTree1.insert(subTree1.end(), tgNode(TASK, *sIter) );
    }
    /*********/
    else if (nChild == 1) // one children
    {
        D1ECHO("Non Leaf case (single child) for " << *sIter);
        i1 = subTree1.insert(subTree1.end(), tgNode(SEQ, *sIter) );
        cgTreeType::sibling_iterator cgIter;
        for(cgIter = cgt.begin(sIter); cgIter != cgt.end(sIter); ++cgIter)
        {
            st1 = GetNodeWithSubtree(cgIter, cgt);
            subTree1.append_child(i1, st1.begin() );
        }
    }
    /*********/
    else // have children
    {
        D1ECHO("Non Leaf case (multi child) for " << *sIter);
        i1 = subTree1.insert(subTree1.end(), tgNode(SEQ, *sIter) );
        cgTreeType::sibling_iterator cgIter;
        for(cgIter = cgt.begin(sIter); cgIter != cgt.end(sIter); ++cgIter)
        {
            st1 = GetNodeWithSubtree(cgIter, cgt);
            subTree1.append_child(i1, st1.begin() );
        }
    }
    return subTree1;
}

void PrintSet(ChildrenSet& set1)
{
    for(auto& elem : set1)
        cout << elem->GetName() << " ";
    cout << endl;
}

void PrintSets(ChildrenSets& sets)
{
    cout << "Sets : \n";
    u32 i=0;
    for(auto& set1 : sets)
    {
        cout << "    Set # " << i++ << " ( " << set1.size() << " ) : ";
        PrintSet(set1);
    }
    cout << endl;
}

bool AreIndependTasks(tgTreeType::iterator it1, tgTreeType::iterator it2)
{
    string prod = it1->GetName();
    string cons = it2->GetName();
    bool result = tdMatrix.AreIndependent( prod, cons );
    D1ECHO("dependence between " << prod << " and " << cons << " : " << result);
    return result;
}

bool AreIndpendSets(ChildrenSet& s1, ChildrenSet& s2)
{
    for(auto& t1 : s1)
    {
        for(auto& t2 : s2)
        {
            if( false == AreIndependTasks(t1,t2) )
            {
                return false;
            }
        }
    }
    return true;
}

void GenerateIndepSets(ChildrenSets& sets, ChildrenSets& isets)
{
    u32 i, j;

    // initialize independent sets with the
    // first element (parent iterator) of each set
    for( i=0; i<sets.size(); i++)
    {
        ChildrenSet set1;
        set1.push_back( sets[i][0] );
        isets.push_back( set1 );
    }
    D1ECHO("After initialization");
    DEXEC( PrintSets(isets) );

    for( i=0; i<sets.size(); i++)
    {
        ChildrenSet& set1 = sets[i];
        for( j=i+1; j<sets.size(); j++)
        {
            ChildrenSet& set2 = sets[j];
            if( AreIndpendSets( set1, set2 ) == true )
            {
                D2ECHO( set2[0]->GetName() << " independent of " << set1[0]->GetName() );
                D1ECHO( VAR(isets[i].size()) << VAR(isets[j].size()));
                isets[i].push_back( isets[j][0] );
                isets[j].erase( isets[j].begin() );
                D1ECHO( VAR(isets[i].size()) << VAR(isets[j].size()));
                DEXEC( PrintSets(isets) );
            }
            else
            {
                D2ECHO( set2[0]->GetName() << " dependent on " << set1[0]->GetName() );
                break;
            }
        }
        i=j-1;
    }
}

u32 GetCountOfNonEmptySets ( ChildrenSets& sets)
{
    u32 i, neCount=0;
    for( i=0; i < sets.size(); i++)
    {
        neCount += ( !(sets[i].empty()) );
    }
    return neCount;
}

void Taskgraph::Parallelize()
{
    D2ECHO("Starting Parallelization ... ");

    tgTreeType::post_order_iterator nodeIter;
    for( nodeIter = tgTree.begin_post(); nodeIter != tgTree.end_post(); ++nodeIter)
    {
        D1ECHO( "Generating independent sets of " << nodeIter->GetName() << " with " << VAR(nodeIter.number_of_children()));

        // generate children sets
        ChildrenSets chsets;

        tgTreeType::sibling_iterator chIter;
        for(chIter = tgTree.begin(nodeIter); chIter != tgTree.end(nodeIter); ++chIter)
        {
            ChildrenSet set1;
            set1.push_back(chIter); // ECHO(*chIter);

            tgTreeType::pre_order_iterator it;
            for( it = tgTree.begin(chIter); it != tgTree.end(chIter); ++it )
            {
                set1.push_back(it); // ECHO(*it);
            }
            chsets.push_back(set1);
        }
        D1ECHO("Print orignal sets");
        DEXEC( PrintSets(chsets) );

        // generate independent sets
        ChildrenSets isets;
        GenerateIndepSets(chsets, isets);
        DECHO("Print current independent sets");
        DEXEC( PrintSets(isets) );
        nodeIter->SetIndependentSets(isets);
    }

#if 0
    ECHO("Printing all independent sets");
    tgTreeType::post_order_iterator nodeIter1;
    for( nodeIter1 = tgTree.begin_post(); nodeIter1 != tgTree.end_post(); ++nodeIter1)
    {
        string n = nodeIter1->GetName();
        ChildrenSets& isets = nodeIter1->GetIndependentSets();
        ECHO("Independent sets of " << n );
        PrintSets(isets);
    }
#endif

    ECHO("Applying parallelization");
    tgTreeType::post_order_iterator nIter;
    for( nIter = tgTree.begin_post(); nIter != tgTree.end_post(); ++nIter)
    {
        string n = nIter->GetName();
        ChildrenSets& isets = nIter->GetIndependentSets();
        u32 countNonEmptyIndepSets = GetCountOfNonEmptySets(isets);
        if( ! isets.empty() )
        {
            D1ECHO("Independent sets of " << n );
            DEXEC( PrintSets(isets) );
            u32 setNo=0;
            for( auto& aset : isets )
            {
                // parallelize the sets with size greater than 1
                if( aset.size() > 1 )
                {
                    D1ECHO("Set to be parallelized ");
                    DEXEC( PrintSet(aset) );

                    if ( countNonEmptyIndepSets == 1) // then just change the type of this node to parallel
                    {
                        D2ECHO("Setting to parallel, as " << VAR(countNonEmptyIndepSets) );
                        nIter->SetType(PAR);
                    }
                    else // in this case we will need to create a dummy node
                    {
                        // create a sub tree with a dummy node
                        tgTreeType st;
                        tgTreeType::iterator sit = st.set_head( tgNode(PAR) );

                        // add this sub tree as child of the node in orignal tree
                        tgTree.move_in_as_nth_child(nIter, setNo, st);

                        // now move the children
                        for(auto& elem : aset)
                        {
                            // remove a child from the orignal tree
                            auto ret = tgTree.move_out(elem);
                            // and add it as child of dummy node in sub tree
                            tgTree.append_child( sit, ret.begin() );
                        }
                    }
                }
                setNo++;
            }
        }
    }


    // collect loops for parallel_for parallelization
    vector<tgTreeType::iterator> parallelFors;
    for( nIter = tgTree.begin_post(); nIter != tgTree.end_post(); ++nIter)
    {
        tgNodeType type = nIter->GetType();
        if ( type == TASK || type == SEQ || type == PAR )
        {
            cgNode node = nIter->GetNode();
            if ( /*node.IsLoop() &&*/ node.IsIndependentLoop() )
            {
                string name = nIter->GetName();
                D1ECHO("Collecting loop : " << name);
                parallelFors.push_back(nIter);
            }
        }
    }

    // Apply parallel_for parallelization
    ECHO("Applying parallel_for parallelization");
    for( auto& pfiter : parallelFors)
    {
        string name = pfiter->GetName();
        D1ECHO("Parallelizing loop : " << name);

        // add a dummy node as child of the node in orignal tree
        D2ECHO("Adding a dummy node of parallel_for type");
        auto diter = tgTree.insert(pfiter, tgNode( PARFOR, name+"_pf") );

        // remove the child (and its children) from the orignal tree
        D2ECHO("removing the orignal child");
        auto ret = tgTree.move_out(pfiter);

        // and add it as child of dummy node
        D2ECHO("attaching the orignal child as child of dummy node");
        tgTree.append_child( diter, ret.begin() );

        D1ECHO("Parallelizing loop. Done ");
    }
}

template<class T>
void print_tree_mxif(const tree<T>& tr,
                     typename tree<T>::pre_order_iterator it,
                     typename tree<T>::pre_order_iterator end,
                     ostream& out
                    )
{
    if(!tr.is_valid(it)) return;
    while(it!=end)
    {
        string name = it->GetName();
        tgNodeType type = it->GetType();
        string typeStr = it->GetTypeString();
        cgNode node = it->GetNode();
        if(type == TASK)
        {
            out << "    ( task " << name << "\n";
            node.GetMXIF(out);
            out << "    )" << endl << flush;
        }
        else if (type == PARFOR)
        {
            out << "    ( task_graph " << name << "\n";
            out << "        ( " << typeStr << "\n";
            // get the child node
            tgTreeType::sibling_iterator tgIter = tr.begin(it);
#if 1       // TODO: Orignally it was like this, Fix it
            if( tgIter != tr.end(it) )
            {
                out << "            ( task " << tgIter->GetName() << ")\n";
            }
#else
            if( tgIter != tr.end(it) )
            {
                cgNode node2 = tgIter->GetNode();
                out << "            ( task " << tgIter->GetName() << ")\n";
                node2.GetMXIF(out);
            }
#endif
            out << "        )" << endl << flush;
            out << "    )" << endl << flush;
        }
        else if (type == SEQ || type == PAR)
        {
            out << "    ( task_graph " << name << "\n";
            out << "        ( " << typeStr << "\n";
            tgTreeType::sibling_iterator tgIter;
            for( tgIter = tr.begin(it); tgIter != tr.end(it); ++tgIter )
            {
                if( tgIter->GetType() == TASK)
                    out << "            ( task " << tgIter->GetName() << ")\n";
                else
                    out << "            ( task_graph " << tgIter->GetName() << ")\n";
            }
            out << "        )" << endl << flush;
            out << "    )" << endl << flush;
        }
        else
        {
            ECHO("Error: Unknown task type");
            Die();
        }
        ++it;
    }
}

void Taskgraph::WriteMXIF(ostream& out)
{
    out << "( parallelization " << appName << "_app" << endl;
    print_tree_mxif(tgTree, tgTree.begin(), tgTree.end(), out);
    out << ")" << endl;
}

void TaskDependenceMatrix::InitFromFile()
{
    ifstream fin;
    string dependFileName("taskdependences.dat");
    OpenInFile(dependFileName, fin);

    string line;
    while ( getline(fin, line) )
    {
        //the following line trims white space from the beginning of the string
        line.erase(line.begin(), find_if(line.begin(), line.end(), not1(ptr_fun<int, int>(isspace))));

        // ignore empty lines and lines starting with #
        if (line.length() == 0 || line[0] == '#')
            continue;

        u32 pid, cid;
        string prod, cons;
        istringstream iss(line);
        if( ! ( iss >> prod >> cons) )
        {
            ECHO("Error");    // error
            break;
        }

        if( Name2ID.find(prod) != Name2ID.end() )
        {
            // found
            pid = Name2ID[prod];
        }
        else
        {
            pid = ID++;
            Name2ID[prod] = pid;
        }

        if( Name2ID.find(cons) != Name2ID.end() )
        {
            // found
            cid = Name2ID[cons];
        }
        else
        {
            cid = ID++;
            Name2ID[cons] = cid;
        }

        u32 size = Matrix.size();
        u32 nsize = ( max(pid,cid) + 1 );
        if(  size < nsize )
        {
            Matrix.resize(nsize);
            for(u32 i=0; i<Matrix.size(); i++)
                Matrix[i].resize(nsize);
        }

        Matrix[pid][cid] = true;
        D1ECHO("Setting Matrix["<< pid <<"]["<< cid <<"] = " << true);
    }
    fin.close();
}

bool TaskDependenceMatrix::AreIndependent(string prod, string cons)
{
    u32 pid, cid;
    D1ECHO("Checking dependencies for " << prod << " and " << cons);
    if( Name2ID.find(prod) != Name2ID.end() ) // found
    {
        pid = Name2ID[prod];
    }
    else
    {
        D1ECHO("Producer not found in dependencies");
        return 1; // if a producer is not found => no dependence
        //Die();  // TODO check later in detail if this is desired
    }

    if( Name2ID.find(cons) != Name2ID.end() ) // found
    {
        cid = Name2ID[cons];
    }
    else
    {
        D1ECHO("Consumer not found in dependencies");
        return 1; // if a producer is not found => no dependence
        //Die();  // TODO check later in detail if this is desired
    }
    return !( Matrix[pid][cid] );
}
