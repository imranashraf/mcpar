/*
 *
 * Authors: Imran Ashraf
 *
 */

#ifndef ENTITIES_H
#define ENTITIES_H

#include "globals.h"
#include "locations.h"

#include <vector>
#include <map>
#include <cstddef>
#include <iomanip>
#include <iostream>
#include <fstream>

using namespace std;

enum entityType {FUNC_ENTITY, LOOP_ENTITY};
const string entityType2Name[] = {"FUNC_ENTITY", "LOOP_ENTITY"};
static map<string,entityType> entityName2Type = { {"FUNC_ENTITY", FUNC_ENTITY},
                                                 {"LOOP_ENTITY", LOOP_ENTITY} };
struct Parameter
{
    string name;
    string type;
};

class Entity
{
private:
    string name;
    entityType type;
    Location startLocation;
    Location endLocation;
    vector<Parameter> inputs;
    vector<Parameter> outputs;
    vector<Location> callSites;

public:
    Entity() {}
    Entity(string n, entityType t, Location sl, Location el, vector<Parameter> ins, vector<Parameter> outs)
    {
        name = n;
        type = t;
        startLocation = sl;
        endLocation = el;
        inputs = ins;
        outputs = outs;
    }
    friend ostream& operator<<( ostream &output, const Entity& e )
    {
        output << e.name << endl;
        output << e.startLocation << endl;
        output << e.endLocation << endl;

        for(auto& in: e.inputs)
            output << in.name << " " << in.type << endl;

        for(auto& out: e.outputs)
            output << out.name << " " << out.type << endl;

        return output;
    }
    void GetMXIF(ostream& output);
    void GetMXIFSimpleFunc(ostream& output);
};

class Entities
{
private:
    map<string, Entity> entities;

public:
    Entities() {}
    void InitFromFile();
    friend ostream& operator<<( ostream &output, const Entities& e )
    {
        for(auto& fpair: e.entities)
        {
            auto &e = fpair.second;
            output << e << endl;
        }
        return output;
    }
    bool Find(string& name, Entity& e)
    {
        bool found;
        map<string, Entity>::iterator it = entities.find(name);
        found = it != entities.end();
        if( found )
            e = it->second;
        return found;
    }
};

#endif
