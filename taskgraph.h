/*
 *
 * Authors: Imran Ashraf
 *
 */

#ifndef TASKGRAPH_H
#define TASKGRAPH_H

#include "globals.h"
#include "tree.h"
#include "tree_util.h"
#include "locations.h"
#include "callgraph.h"

#include <vector>
#include <cstddef>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <map>
#include <string>

using namespace std;

enum tgNodeType {TASK, SEQ, PAR, PARFOR};
const string tgNodeTypeName[] = {"task","sequential","parallel", "parallel_for"};

class tgNode;
typedef tree<tgNode> tgTreeType;
typedef vector<tgTreeType::iterator> ChildrenSet;
typedef vector<ChildrenSet> ChildrenSets;

class tgNode
{
private:
    tgNodeType type;
    cgNode node;
    string name;
    ChildrenSets indepSets;
    static u32 PARTASKCOUNTER;

public:
    tgNode(): type(TASK) {}
    tgNode(tgNodeType t): type(t)
    {
        name = "dummy_par_" + to_string(PARTASKCOUNTER++);
    }
    tgNode(tgNodeType t, string n): type(t), name(n) {}
    tgNode(tgNodeType t, cgNode cgn): type(t), node(cgn)
    {
        name = cgn.GetName();
    }
    string GetName()
    {
        return name;
    }
    tgNodeType GetType()
    {
        return type;
    }
    void SetType( tgNodeType t)
    {
        type=t;
    }
    string GetTypeString()
    {
        return tgNodeTypeName[type];
    }
    cgNode GetNode()
    {
        return node;
    }
    friend ostream& operator<<( ostream &output, const tgNode& tgn )
    {
        output << tgNodeTypeName[tgn.type] << " ";
        output << tgn.name;
        return output;
    }
    void SetIndependentSets( ChildrenSets sets)
    {
        indepSets = sets;
    }
    ChildrenSets& GetIndependentSets()
    {
        return indepSets;
    }
};

class Taskgraph
{
private:
    tgTreeType tgTree;
    string appName;

public:
    Taskgraph() {}
    Taskgraph(Callgraph& cg)
    {
        appName = cg.getName();
        cgTreeType cgt = cg.getTree();
        tgTree =  GetNodeWithSubtree(cgt.begin(), cgt);
    }

    tgTreeType GetNodeWithSubtree(cgTreeType::sibling_iterator sIter, cgTreeType& cgt);
    void Parallelize();
    friend ostream& operator<<( ostream &output, const Taskgraph& tg )
    {
        print_tree(tg.tgTree, tg.tgTree.begin(), tg.tgTree.end(), output);
        output << endl;
        return output;
    }
    void WriteMXIF(ostream& out);
};

class TaskDependenceMatrix
{
private:
    vector< vector<bool> >Matrix;
    map<string,u32> Name2ID;
    static u32 ID;

public:
    TaskDependenceMatrix() {}
    void InitFromFile();
    bool AreIndependent(string prod, string cons);

};

#endif
