/*
 *
 * Authors: Imran Ashraf
 *
 */

#include "globals.h"
#include "entities.h"
#include "callgraph.h"
#include "taskgraph.h"

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <vector>

using namespace std;
Entities entities;
Locations locs;
TaskDependenceMatrix tdMatrix;
u32 TaskDependenceMatrix::ID = 0;
u32 tgNode::PARTASKCOUNTER=1;

int main(int argc, char** argv)
{
    ECHO("MXIF Generator");
    string appName("default");
    if(argc > 1 ) appName = argv[1];
    entities.InitFromFile();
    //ECHO("Printing all entities");
    //cout << entities;

    locs.InitFromFile();
//     ECHO( "Printing all locations");
//     cout << locs;


    Callgraph cg(appName);
    cg.InitFromFile();
    ECHO("Printing callgraph ");
    cout << cg;

    Taskgraph tg(cg);
    ECHO("Printing initial taskgraph");
    cout << tg;

//     ECHO("Printing taskgraph in MXIF");
//     tg.WriteMXIF();

    // initialize task dependence matrix
    tdMatrix.InitFromFile();

//     ECHO("Parallelizing taskgraph");
    tg.Parallelize();

    ECHO("Printing parallel taskgraph");
    cout << tg;

    {
        ofstream fout;
        string parFileName( appName + "_parallel.dat" );
        ECHO("Printing parallel taskgraph in plain format in " << parFileName);
        OpenOutFile(parFileName, fout);
        fout << tg;
        ECHO("Successfully generated " << parFileName );
        fout.close();
    }

    {
        ofstream fout;
        string mxifFileName( appName + ".mxif" );
        ECHO("Printing parallel taskgraph in MXIF format in " << mxifFileName);
        OpenOutFile(mxifFileName, fout);
        tg.WriteMXIF(fout);
        ECHO("Successfully generated " << mxifFileName );
        fout.close();
    }

    return 0;
}
