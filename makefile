CXX     = g++
CFLAGS  = -O3 -Wall -std=c++11  -MD -Wfatal-errors
INCS    = -I.
LPATHS  = -L. 
LIBS    = -lm
LFLAGS  =

SRCS	= mcpar.cpp globals.cpp entities.cpp locations.cpp callgraph.cpp taskgraph.cpp

OBJS	=$(SRCS:%.cpp=%.o)
EXEC    = mcpar
CMD	=./$(EXEC)


#all: $(EXEC) run

$(EXEC) : $(OBJS)
	$(CXX) $(OBJS) -o $(EXEC) $(LPATHS) $(LIBS) $(LFLAGS)

%.o : %.cpp
	$(CXX) $(CFLAGS) $(INCS) -c $< -o $@
	@cp $*.d $*.P; \
	sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
	-e '/^$$/ d' -e 's/$$/ :/' < $*.d >> $*.P; \
	rm -f $*.d

-include *.P

run: $(EXEC)
	$(CMD)

debug: CFLAGS  = -Og -g -Wall -std=c++11  -MD
debug: $(EXEC)
# 	gdb $(CMD)

clean:
	rm -f $(EXEC) $(OBJS) *~ *.P

format:
	astyle --style=allman *.h *.cpp
	rm -f *.orig

openingFileOrder = makefile globals.h globals.cpp locations.h locations.cpp \
		 entities.h entities.cpp tree.h tree_util.h \
		 callgraph.h callgraph.cpp taskgraph.h taskgraph.cpp \
		 mcpar.cpp entities.dat locations.dat callgraph.dat dependencies.dat

open:
	kate -n $(openingFileOrder) &> /dev/null &

vimopen:
	vim -p $(openingFileOrder)

tarball:
	make clean
	cd ..; tar -czf mcpar.tar.gz  --exclude='mcpar/.git*' mcpar
	mv ../mcpar.tar.gz ~/Desktop/.

.PHONY: clean run all open format
