/*
 *
 * Authors: Imran Ashraf
 *
 */

#include "callgraph.h"

#include <sstream>
#include <string>

void Callgraph::InitFromFile()
{
    // first read dependency file name to get loops with independent loop iterations
    ECHO("Reading independent loops");
    vector<string> indepLoops;
    ifstream indlfin;
    string indepLoopFileName("independentloops.dat");
    OpenInFile(indepLoopFileName, indlfin);
    string line;
    while ( getline(indlfin, line) )
    {
        //the following line trims white space from the beginning of the string
        line.erase(line.begin(), find_if(line.begin(), line.end(), not1(ptr_fun<int, int>(isspace))));

        // ignore empty lines and lines starting with #
        if (line.length() == 0 || line[0] == '#')
            continue;

        string indeploop;
        istringstream iss(line);
        while ( (iss >> indeploop) ) 
        {
            indepLoops.push_back(indeploop);
            D1ECHO("Read independent loop : " << indeploop);
        }
    }
    indlfin.close();

    // read the callgraph
    ifstream fin;
    string callgraphFileName("callgraph.dat");
    OpenInFile(callgraphFileName, fin);

    //string line;
    while ( getline(fin, line) )
    {
        //the following line trims white space from the beginning of the string
        line.erase(line.begin(), find_if(line.begin(), line.end(), not1(ptr_fun<int, int>(isspace))));

        // ignore empty lines and lines starting with #
        if (line.length() == 0 || line[0] == '#')
            continue;

        string typeStr;
        string callerName;
        cgNodeType type;
        string calleeName;

        istringstream iss(line);
        if ( !(iss >> callerName ) )
        {
            ECHO("Error");    // error
            break;
        }

        // FUNC will be used as default type of caller, if not found (below).
        // if found, actual type (already set) will be used
        type = FUNC;
        cgNode caller(type, callerName);
        D1ECHO(caller);

        if ( !(iss >> typeStr >> calleeName ) )
        {
            ECHO("Error");    // error
            break;
        }

        type = (typeStr=="FUNC")?FUNC:LOOP;
        cgNode callee(type, calleeName);
        D1ECHO(callee);
        if(type ==LOOP)
        {
            auto itl = find(indepLoops.begin(), indepLoops.end(), calleeName);
            if( itl != indepLoops.end() )
            {
                ECHO(calleeName << " set as independent loop");
                callee.SetAsIndependentLoop();
            }
            else
            {
                ECHO(calleeName << " set as dependent loop");
            }
        }

        cgTreeType::pre_order_iterator it;
        it = find( callTree.begin(), callTree.end(), caller );
        if( it != callTree.end() )
        {
            // found caller, so add callee as child
            callTree.append_child(it, callee);
        }
        else
        {
            // caller not found, so add caller and then add callee as its child
            it = callTree.insert(callTree.begin(), caller);
            callTree.append_child(it, callee);
        }
    }

    fin.close();
}
