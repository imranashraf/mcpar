/*
 *
 * Authors: Imran Ashraf
 *
 */

#include "locations.h"
#include <algorithm>
#include <sstream>
#include <string>

void Locations::InitFromFile()
{
    ifstream fin;
    string locationsFileName("locations.dat");
    OpenInFile(locationsFileName, fin);

    u32 counter=0;
    string line;
    while ( getline(fin, line) )
    {
        //the following line trims white space from the beginning of the string
        line.erase(line.begin(), find_if(line.begin(), line.end(), not1(ptr_fun<int, int>(isspace))));

        // ignore empty lines and lines starting with #
        if (line.length() == 0 || line[0] == '#')
            continue;

        string filename;
        u32 lno;
        istringstream iss(line);
        if (!(iss >> filename >> lno))
        {
            break;    // error
        }
        Insert( Location(filename, lno) );
        ++counter;
    }
    fin.close();

    if(counter==0)
    {
        ECHO("No location available in the input file.");
        Die();
    }

    ECHO("Initialized " << counter << " locations from file");
}
