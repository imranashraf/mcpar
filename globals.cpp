/*
 *
 * Authors: Imran Ashraf
 *
 */

#include "globals.h"

#include <cmath>
#include <cstring>
#include <map>

using namespace std;

IDNoType GlobalID=UnknownID;
char currDir[FILENAME_MAX];

bool isEmpty(ifstream& fin)
{
    return fin.peek() == ifstream::traits_type::eof();
}

void OpenInFile(const string& fileName, ifstream& fin)
{
    if (!fileName.empty())
    {
        fin.open(fileName.c_str());
        if ( fin.fail() )
        {
            ECHO("Can not open input file (" <<fileName.c_str() << "). Aborting...");
            Die();
        }
    }
    else
    {
        ECHO("Specify a non empty file name. Aborting ...");
        Die();
    }

//     if(isEmpty(fin))
//     {
//         ECHO("Input file (" <<fileName.c_str()<<") is empty. Aborting...");
//         Die();
//     }
}

void OpenOutFile(const string& fileName, ofstream& fout)
{
    if (!fileName.empty())
    {
        fout.open(fileName.c_str(), ios::binary);
        if ( fout.fail() )
        {
            ECHO("Can not open output file (" << fileName.c_str() << "). Aborting ...");
            Die();
        }
    }
    else
    {
        ECHO("Specify a non empty file name. Aborting ...");
        Die();
    }
}

inline bool IsPowerOfTwo(uptr x)
{
    return (x & (x - 1)) == 0;
}

inline uptr RoundUpTo(uptr size, uptr boundary)
{
    CHECK(IsPowerOfTwo(boundary));
    return (size + boundary - 1) & ~(boundary - 1);
}

inline uptr RoundDownTo(uptr x, uptr boundary)
{
    return x & ~(boundary - 1);
}

inline bool IsAligned(uptr a, uptr alignment)
{
    return (a & (alignment - 1)) == 0;
}

const std::string& humanReadableByteCount(u64 bytes, bool si)
{
    string hBytes;
    char cBytes[100];
    u16 unit = si ? 1000 : 1024;
    string punits1 = (si ? "kMGTPE" : "KMGTPE");
    char punits2 = (si ? '\0' : 'i');
    char pre[3];

    if (bytes < unit)
        hBytes = to_string((long long)bytes) + " B";
    else
    {
        int exp = (int) (log(bytes) / log(unit));
        pre[0] = punits1[exp-1];
        pre[1] = punits2;
        pre[2] = '\0';
        float val = bytes / pow(unit, exp);
        sprintf(cBytes, "%.1f %sB", val, pre);
        hBytes = cBytes;
    }

    return *new string(hBytes);
}

const std::string& hBytes(u64 bytes)
{
    return humanReadableByteCount(bytes, true);
}

void SetCurrDir()
{
    if (!GetCurrentDir(currDir, sizeof(currDir)))
    {
        ECHO("Error setting current dir ");
        Die();
    }
    //cCurrentPath[sizeof(cCurrentPath) - 1] = '\0';
    strcat(currDir, "/");
}

void PrintCurrDir()
{
    ECHO("The current working directory is " << currDir);
}

void RemoveSubstrs(std::string& src, std::string& toRemove)
{
    int n = toRemove.length();
    std::size_t i;
    for( i = src.find(toRemove); i != string::npos; i = src.find(toRemove) )
        src.erase(i, n);
}

void RemoveCurrDirFromName(std::string& src)
{
    std::string cdir(currDir);
    RemoveSubstrs(src, cdir);
}

void ExtractNoFromName(string& name, u32& no)
{
    size_t found = name.find_last_of("_");
    if( found != string::npos )
    {
        string nostr = name.substr(found+1);
        no = stoul( nostr , nullptr , 0);
        name =  name.substr(0,found);
        D3ECHO(" name: " << name);
        D3ECHO(" no: " << no );
    }
}
