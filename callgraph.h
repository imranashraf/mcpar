/*
 *
 * Authors: Imran Ashraf
 *
 */

#ifndef CALLGRAPH_H
#define CALLGRAPH_H

#include "globals.h"
#include "entities.h"
#include "tree.h"
#include "tree_util.h"
#include "locations.h"

#include <vector>
#include <cstddef>
#include <iomanip>
#include <iostream>

using namespace std;

extern Entities entities;
extern Locations locs;

enum cgNodeType {FUNC, LOOP};
const string cgNodeTypeName[] = {"FUNC", "LOOP"};

class cgNode
{
private:
    cgNodeType type;
    string name;
    bool isIndependentLoop; // i.e. loop which has indepenent iterations

public:
    cgNode() {isIndependentLoop=false;}

    cgNode(cgNodeType t, string n) : type(t), name(n) {isIndependentLoop=false;}

    bool IsLoop() {return (type == LOOP);}
    void SetAsIndependentLoop() { isIndependentLoop=true;};
    bool IsIndependentLoop(){ return isIndependentLoop; }
    string GetName() {return name;}
    bool operator<(const cgNode& c) { return (name < c.name); }
    bool operator==(const cgNode& c) { return ( name == c.name); }

    friend ostream& operator<<( ostream &output, cgNode& c )
    {
        u32 cidx = 0;
        string cname = c.GetName();
        ExtractNoFromName(cname, cidx);
        ExtractNoFromName(cname, cidx);
        Location loc = locs.GetLocation( cidx );
        output << cgNodeTypeName[c.type] << " " << c.name << "(" << loc << ")";
        return output;
    }

    void GetMXIF(ostream& out)
    {
        out << "        ( mapping_of" << "\n";
        Entity e;
        string cname = name;
        u32 cidx1 = 0;
        D2ECHO("Entity name before extraction : " << name);
        ExtractNoFromName(cname, cidx1); // remove id
        if( type == FUNC)
            ExtractNoFromName(cname, cidx1); // remove/get last call loc idx
        if( true == entities.Find(cname, e) )
        {
            e.GetMXIF(out);
        }
        else
        {
            ECHO("Error: Entry (function/loop) " << cname << " not found" );
            Die();
        }
        if( type == LOOP )
            ExtractNoFromName(cname, cidx1); // remove/get last call loc idx
        Location loc = locs.GetLocation( cidx1 );
        out << "        )\n"; // end mapping_of
        out << "        ( call_file \"" << loc.GetFile() << "\" )\n";
        out << "        ( call_line " << loc.GetLine() << " )\n";
    }
};

typedef tree<cgNode> cgTreeType;
class Callgraph
{
private:
    cgTreeType callTree;
    string appName;

public:
    Callgraph() {}
    Callgraph(string app) : appName(app) {}
    string getName() { return appName; }
    cgTreeType getTree() { return callTree; }
    void InitFromFile();
    friend ostream& operator<<( ostream &output, const Callgraph& cg )
    {
        print_tree(cg.callTree, cg.callTree.begin(), cg.callTree.end(), output);
        output << endl;
        return output;
    }
};

#endif
