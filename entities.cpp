/*
 *
 * Authors: Imran Ashraf
 *
 */

#include "entities.h"

#include <sstream>
#include <string>
#include <algorithm>

extern Entities entities;

void Entity::GetMXIF(ostream& output)
{
    string cname = name;
    u32 cidx=0;
    output << "            (";
    if ( FUNC_ENTITY == type )
    {
        output << " function " << name << "\n";
        output << "                ( file \"" << startLocation.GetFile() << "\" )" << endl;
        output << "                ( lines " << startLocation.GetLine() << " " << endLocation.GetLine() << " )" << endl;
    }
    else
    {
        output << " chunk_of " << name << "\n";

        // in case of loop we also need to print the info of function of loop
        Entity e;
        ExtractNoFromName(cname, cidx);
        D1ECHO("loop is in function : " << cname);
        if( true == entities.Find(cname, e) )
        {
            e.GetMXIFSimpleFunc(output);
        }
        else
        {
            ECHO("Error: Entry (function) " << cname << " not found" );
            Die();
        }
        output << "                ( lines " << startLocation.GetLine() << " " << endLocation.GetLine() << " )" << endl;
    }

    for(auto& in: inputs)
    {
        if(in.type == "")
            output << "                ( input " << in.name << " )" << endl;
        else
            output << "                ( input " << in.name << " \"" << in.type << " \" )" << endl;
    }

    for(auto& out: outputs)
    {
        if(out.type == "" )
            output << "                ( output " << out.name << " )" << endl;
        else
            output << "                ( output " << out.name << " \"" << out.type << " \" )" << endl;
    }

    output << "            )\n";
}

// this is used to print info of parent function of loop
void Entity::GetMXIFSimpleFunc(ostream& output)
{
    string cname = name;
    output << "                (";
    if ( FUNC_ENTITY == type )
    {
        output << " function " << name << "\n";
    }
    else
    {
        ECHO("Error: Only Entity of type function is expected to call this function");
        Die();
    }

    output << "                   ( file \"" << startLocation.GetFile() << "\" )" << endl;
    output << "                   ( lines " << startLocation.GetLine() << " " << endLocation.GetLine() << " )" << endl;
    output << "                )\n";
}

void Entities::InitFromFile()
{
    ifstream fin;
    string eFileName("entities.dat");
    OpenInFile(eFileName, fin);

    u32 i=0;
    string line;

    while ( getline(fin, line) )
    {
        //the following line trims white space from the beginning of the string
        line.erase(line.begin(), find_if(line.begin(), line.end(), not1(ptr_fun<int, int>(isspace))));

        // ignore empty lines and lines starting with #
        if (line.length() == 0 || line[0] == '#')
            continue;

        string token;
        string ename;
        string filename;
        string etypestr;
        u32 sline, eline;
        vector<Parameter> ins;
        vector<Parameter> outs;

        // get entity type string
        etypestr = line;

        // get name
        getline(fin, line);
        ename = line;

        // get start location
        getline(fin, line);
        istringstream iss1(line);
        if (!(iss1 >> filename >> sline))
        {
            break;    // error
        }
        Location sloc(filename, sline);

        // get end location
        getline(fin, line);
        istringstream iss2(line);
        if (!(iss2 >> filename >> eline))
        {
            break;    // error
        }
        Location eloc(filename, eline);

        // process inputs
        getline(fin, line);
        istringstream isin(line);
        while ( getline(isin, token, ',') )
        {
            Parameter par;
            string name, type;
            istringstream issins(token);
            if ( !(issins >> par.name) ) break;
            if ( par.name != "NULL" )
            {
                string temp;
                while (issins >> temp) 
                    par.type += " " + temp;;
            }
            ins.push_back(par);
        }

        // process oututs
        getline(fin, line);
        istringstream isout(line);
        while ( getline(isout, token, ',') )
        {
            Parameter par;
            string name, type;
            istringstream issouts(token);
            if ( !(issouts >> par.name) ) break;
            if ( par.name != "NULL" )
            {
                string temp;
                while (issouts >> temp) 
                    par.type += " " + temp;;
            }
            outs.push_back(par);
        }

        entityType etype = entityName2Type[etypestr];
        Entity e(ename, etype, sloc, eloc, ins, outs);
        entities[ename] = e;
        i++;
        D2ECHO("Initialized entity : " << e);
    }

    fin.close();

    if(i==0)
    {
        ECHO("No entities available in the input file.");
        Die();
    }
    ECHO("Initialized " << i << " entities from file");
}
