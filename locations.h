/*
 *
 * Authors: Imran Ashraf
 *
 */

#ifndef LOCATIONS_H
#define LOCATIONS_H

#include "globals.h"

#include <vector>
#include <cstddef>
#include <iomanip>
#include <iostream>

using namespace std;

class Location
{
private:
    string file;
    u32 line;

public:
    Location() {}
    Location(const string& f, u32 l): file(f), line(l) {}
    u32 GetLine()
    {
        return line;
    }
    string GetFile()
    {
        return file;
    }
    bool operator< (const Location& l)
    {
        return (file < l.file) && ( line < l.line);
    }
    bool operator== (const Location& l)
    {
        return (file == l.file) && (line == l.line);
    }
    friend ostream& operator<< ( ostream &output, const Location& l )
    {
        output << l.file << ":" << l.line;
        return output;
    }
};

class Locations
{
private:
    vector<Location> locations;
    // NOTE location index 0 is for unknown location
public:
    Locations() {}

    // Insert a location and return location index
    u16 Insert(Location loc)
    {
        locations.push_back(loc);
        return locations.size()-1;  // loc index of currently inserted size -1
    }

    Location& GetLocation(u16 index)
    {
        if ( index < locations.size() )
            return locations.at(index);
        else
        {
            ECHO("Location index out of range");
            Die();
        }
    }

    friend ostream& operator<<( ostream &output, const Locations& locs )
    {
        for(auto& l: locs.locations)
            output << l << endl;

        return output;
    }
    void InitFromFile();
};

#endif
